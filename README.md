# CubeSat LSTM

This repo exists as a collection of the scripts used to:
  - Train the LSTM model
  - Fit the SARIMA model
  - Generate the power consumption data (Flight planner)
  - Simulate the regulated charging of a battery (CMC)

See appropriate sub folders for the above mentioned scripts.

## LSTM Model
Included here is the Train.py script along with relevant training, validation, and testing data. Furthermore, the model trained for the project can be found as 'FinalModel.pth'. For the validation set, there exists multiple versions. This is due to the fact that the training process includes early stopping which requires a different validation set. All files are included which allows the code to run as is.

Train.py is setup to optimize hyperparameters for an LSTM model and train the model using those parameters. The hyperparameters which are being tuned and their values are found as parameters in the top of the script.

The script should be used by tuning a model with the Adam or SGD optimizer using the 'findOptimModel' fundtion, followed by training a final model using the 'trainTestFinalModel' function. In the tuning process only the training data is used to train the model, while the final training uses both the training and validation data.

To test the model's performance, compare the predictions found in 'Resulsts.csv' with the test set found in 'testSet.csv'.

## SARIMA Model
Included here is the FitArima.m and predict.m along with relevant training, validation and testing data (same data as found with the LSTM model).

FitArima.m is setup to Fit a SARIMA model to each individual channel in the training data. This results in a list of parameters in the local table 'params'.

predict.m uses the parameters found in the fitting to predict generation data. The resulting predictions are saved in the file 'arimaResult.csv' and the parameters used in the 'params.csv'.

## Flight Planner
Included here is the files 'Channel5DataGen.py' and 'Channel6DataGen.py'.

Both files are setup to generate power consumption data which represents the data seen in the telemetry data. The exact values used to generate the data was found in a separate analysis of the telemetry data.

The scripts each result in a csv file for the respective channel. These files can be found in the CMC folder, however, in a combined file.

## CMC
Included here is the cmc.py along with the prediction data from the LSTM and the SARIMA models, 'results.csv' and 'arimaResult.csv' respectively as well as the power consumption data, 'powerConsump.csv', and the test set, 'testSet.csv'. Furthermore, the file 'soc_ocv_25degc.csv' can be found. This file is used as a lookup when charging the battery.

cmc.py simulates charging over the predicted periods using either of the model's predictions, unregulated charging, or ideal charging.

The resulting charging data is finally plotted.
