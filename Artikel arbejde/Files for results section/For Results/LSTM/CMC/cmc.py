"""
CMC (Charging Multiplier Calculator) and battery model
"""
#-Libraries---------------------------------------------------------------------

import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
from torch.autograd import Variable

#-MatPlotLib configuration------------------------------------------------------

plt.rcParams['figure.figsize'] = (20.0, 10.0)
plt.rcParams.update({'font.size': 34})
plt.style.use('seaborn-whitegrid')

#-Global variables--------------------------------------------------------------

simTS = 10 #[Hz] sample frequency of data
periodLength = 567 # rounded down to nearest multiple of 10
simLength = 21*periodLength #[s] length of sunlight period / simulation
sunPeriod = 367

#-Import data-------------------------------------------------------------------

# Power production and consumption from CSV file to arrays
def readDataLSTM(period):

    actual = pd.read_csv("testSet.csv")
    pred = pd.read_csv("Results.csv")
    powerOut = pd.read_csv("powerConsump.csv")

    return actual["total"].values.tolist(), pred["total"].values.tolist(), powerOut["total"].values.tolist()


def readDataSARIMA(period):

    actual = pd.read_csv("testSet.csv")
    pred = pd.read_csv("arimaResult.csv")
    powerOut = pd.read_csv("powerConsump.csv")

    return actual["total"].values.tolist(), pred["total"].values.tolist(), powerOut["total"].values.tolist()



# load file storing SOC-OCV lookup table generated for 25degC
ocv_lookup = pd.read_csv("soc_ocv_25degc.csv", skiprows = 1,
                names =['SOC','OCV','OCV4'],index_col=False,engine='python')

#-BatteryPack class definition--------------------------------------------------

class BatteryPack:
    cell_Rs = 0.075 #[Ohm] Resistance of a cell measured at 25degC after 20s pulse
    cellCapacity = 2.6 #[Ah] Considered capacity of a cell at beggining of life
    OCV = 0 # OCV for 1 cell
    SOC = 0

    # Constructor function
    def __init__(self, serial, parallel, initSOC):

        self.serialCells = serial
        self.parallelCells = parallel
        self.Rs = (self.cell_Rs*self.serialCells)/self.parallelCells
        self.capacity = parallel*self.cellCapacity #[Ah] capacity of battery pack
        self.prev_SOC = initSOC #[0-1] set initial state of charge
        self.OCV = np.interp(initSOC, ocv_lookup.SOC, ocv_lookup.OCV4)
        self.voltage = self.OCV

    # Ideal load calculator
    def calculateIdealLoad(self, goalSOC, initialSOC):
        idealILs = []
        # Simulate an entire sun period
        for i in range (int(sunPeriod/simTS)):
            f = (3600*(self.prev_SOC-goalSOC)*self.capacity)/(3670-(i*10))
            idealILs.append(f)

            self.voltage = self.OCV - self.Rs * idealILs[-1]
            self.SOC = self.prev_SOC - (idealILs[-1] * simTS)/(self.capacity * 3600)
            self.prev_SOC = self.SOC

        #calculate ideal current load average
        idealIL = np.average(idealILs)

        #revert batterypack to its state before ideal load calculations
        self.prev_SOC = initialSOC # set initial state of charge
        self.OCV = np.interp(initialSOC, ocv_lookup.SOC, ocv_lookup.OCV4)
        self.voltage = self.OCV

        return idealIL

    def calculateCM(self, timeCounter, predictedMaxLoad, idealLoad):

        CMC = idealLoad/((predictedMaxLoad/self.voltage)/self.parallelCells)
        # ensure that cmc is always between 0 and 1.
        # in cases where cmc is not, default value is 1.
        if(CMC > 1):
            CMC = 1
        if(CMC < 0):
            CMC = 1

        # print("Max load: " , predictedMaxLoad)
        # print("Ideal load: " , idealLoad)
        # print("CMC: " , CMC)

        return CMC

    def charge(self, input, timeStep, debugIL):

        IL = (input/self.voltage)/self.parallelCells
        self.OCV = np.interp(self.prev_SOC, ocv_lookup.SOC, ocv_lookup.OCV4)

        if (self.prev_SOC - (IL * timeStep)/(self.capacity * 3600)) > 0.85:
            IL = (3600*(-0.85+self.prev_SOC)*self.capacity)/timeStep # calculate IL if SOC should be 1
            self.SOC = 0.85
        else:
            self.SOC = self.prev_SOC - (IL * timeStep)/(self.capacity * 3600)

        self.voltage = self.OCV - self.Rs * IL
        self.prev_SOC = self.SOC
        debugIL.append(IL)



def predictionCharging(model):
    debugIL = []
    outdata_time = [] # Output array for timekeeping
    outdata_soc = [] # State of charge output array
    cmc_out = []
    counter = 0
    battery = BatteryPack(4,2,0.75)
    inputPeriods = 1

    if model == 'SARIMA':
        actual, pred, out = readDataSARIMA(counter)
    elif model == 'LSTM':
        actual, pred, out = readDataLSTM(counter)
    else:
        print("Wrong input, please specify the model to use (SARIMA, LSTM)")
        return

    for globalTime in range(0, simLength, periodLength):
        # print(counter)
        # get prediction data for the next orbit
        startPred = int(globalTime)
        endPred =int((globalTime)+(periodLength))
        Prediction = pred[startPred:endPred]

        # get actual data for the next orbit
        startActual = int(globalTime+((periodLength*inputPeriods)))
        endActual = int((globalTime)+(periodLength)+(periodLength*inputPeriods))
        powerIn = actual[startActual:endActual]
        powerOut = out[startPred:endPred]

        currentLoad = battery.calculateIdealLoad(0.85, battery.prev_SOC)
        # print("\nIdeal load: %f" % currentLoad)
        for i in range(int(periodLength)):
            predictedInput = powerOut[i] - Prediction[i]
            CMC = battery.calculateCM((i + int(globalTime)), predictedInput, currentLoad)
            cmc_out.append(CMC)

            realInput = (powerOut[i] - powerIn[i])*CMC

            battery.charge(realInput, simTS, debugIL)
            outdata_time.append(i * simTS + globalTime*simTS)
            outdata_soc.append(battery.SOC)

        counter += 1
    #-Plot output-------------------------------------------------------------------
    plt.hlines(0.85, 0, len(outdata_time)*simTS, linestyles='dashed', label='SOC limit', color = '#D55E00')
    plt.plot(outdata_time, outdata_soc,  label='SOC', color = '#D55E00')
    plt.plot(outdata_time, debugIL,  label='IL [A]', color = '#0072B2')
    plt.plot(outdata_time, cmc_out,  label='CMC', color = '#F0E442')
    plt.xlabel('Time [s]')
    if model == 'SARIMA':
        plt.title('B) SARIMA')
    elif model == 'LSTM':
        plt.title('C) LSTM')

    plt.ylim(-0.5, 1.05)
    # plt.legend()
    plt.show()
    # plt.savefig("LSTM_CMC.pdf", bbox_inches="tight")


def baseLineCharging():
    debugIL = []
    outdata_time = [] # Output array for timekeeping
    outdata_soc = [] # State of charge output array
    cmc_out = []
    counter = 0
    battery = BatteryPack(4,2,0.75)
    inputPeriods = 1

    actual, pred, out = readDataLSTM(counter)
    for globalTime in range(0, simLength, periodLength):
        # print(counter)

        # get data for the next orbit
        startPred = int(globalTime)
        endPred =int((globalTime)+(periodLength))
        startActual = int(globalTime+((periodLength*inputPeriods)))
        endActual = int((globalTime)+(periodLength)+(periodLength*inputPeriods))
        powerIn = actual[startActual:endActual]
        powerOut = out[startPred:endPred]

        currentLoad = battery.calculateIdealLoad(0.85, battery.prev_SOC)
        # print("\nIdeal load: %f" % currentLoad)
        for i in range(int(periodLength)):

            realInput = (powerOut[i] - powerIn[i])

            battery.charge(realInput, simTS, debugIL)
            outdata_time.append(i * simTS + globalTime*simTS)
            outdata_soc.append(battery.SOC)

        counter += 1
    #-Plot output-------------------------------------------------------------------
    plt.hlines(0.85, 0, len(outdata_time)*simTS, linestyles='dashed', label='SOC limit', color = '#D55E00')
    plt.plot(outdata_time, outdata_soc,  label='SOC', color = '#D55E00')
    plt.plot(outdata_time, debugIL,  label='IL [A]', color = '#0072B2')
    plt.xlabel('Time [s]')
    plt.title('A) Unregulated')
    plt.ylim(-0.5, 1.05)
    # plt.legend()
    plt.show()
    # plt.savefig("Unregulated_CMC.pdf", bbox_inches="tight")


def perfectCharging():
    debugIL = []
    outdata_time = [] # Output array for timekeeping
    outdata_soc = [] # State of charge output array
    cmc_out = []
    counter = 0
    battery = BatteryPack(4,2,0.75)
    inputPeriods = 1

    actual, pred, out = readDataLSTM(counter)
    for globalTime in range(0, simLength, periodLength):
        # print(counter)

        # get data for the next orbit
        startPred = int(globalTime)
        endPred =int((globalTime)+(periodLength))
        startActual = int(globalTime+((periodLength*inputPeriods)))
        endActual = int((globalTime)+(periodLength)+(periodLength*inputPeriods))
        powerActual = actual[startActual:endActual]
        powerIn = actual[startActual:endActual]
        powerOut = out[startPred:endPred]

        currentLoad = battery.calculateIdealLoad(0.85, battery.prev_SOC)
        # print("\nIdeal load: %f" % currentLoad)
        for i in range(int(periodLength)):
            ActualInput = powerOut[i] - powerActual[i] # this is the ideal charging equivalent to the predicted Input from predictionCharging().

            CMC = battery.calculateCM((i + int(globalTime)), ActualInput, currentLoad)
            cmc_out.append(CMC)
            realInput = (powerOut[i] - powerIn[i])*CMC

            battery.charge(realInput, simTS, debugIL)
            outdata_time.append(i * simTS + globalTime*simTS)
            outdata_soc.append(battery.SOC)

        counter += 1
    #-Plot output-------------------------------------------------------------------
    plt.hlines(0.85, 0, len(outdata_time)*simTS, linestyles='dashed', label='SOC limit', color = '#D55E00')
    plt.plot(outdata_time, outdata_soc,  label='SOC', color = '#D55E00')
    plt.plot(outdata_time, debugIL,  label='IL [A]', color = '#0072B2')
    plt.plot(outdata_time, cmc_out,  label='CMC', color = '#F0E442')
    plt.xlabel('Time [s]')
    plt.title('D) Ideal')
    plt.ylim(-0.5, 1.05)
    # plt.legend()
    plt.show()
    # plt.savefig("Ideal_CMC.pdf", bbox_inches="tight")

if __name__ == '__main__':

    predictionCharging('LSTM')
    predictionCharging('SARIMA')
    baseLineCharging()
    perfectCharging()
