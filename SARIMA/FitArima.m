params = [];
trainingSet = readtable("trainingSet.csv");
validationSet = readtable("validationSet.csv");
for chan=2:7
    fitData = table2array(trainingSet(:,chan));
    loss = [];
    valData = table2array(validationSet(1:567,chan));
    valData2 = table2array(validationSet(568:567*2,chan));
    for p=0:5
        for d=0:5
            for q=0:5
                try
                    Mdl = arima(p,d,q);
                    Mdl.Seasonality = 567-p;
                    EstMdl = estimate(Mdl,fitData, 'Display','off');
                    [pred,e] = forecast(EstMdl, 567, valData);
                    loss(p+1,d+1,q+1) = immse(pred, valData2);
                catch
                    loss(p+1,d+1,q+1) = 99999999999999;
                end
            end
        end
    end

    lowestLoss = 99999999999999;
    index = [];
    for p=1:6
        for d=1:6
            for q=1:6
                if loss(p,d,q) < lowestLoss
                    lowestLoss = loss(p,d,q);
                    index = [p-1,d-1,q-1];
                end
            end
        end
    end
    index
    params(chan-1,1) = index(1);
    params(chan-1,2) = index(2);
    params(chan-1,3) = index(3);
 
end

