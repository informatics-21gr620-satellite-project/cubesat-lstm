% This code is meant to be run after FitArima.m while the params table is
% still in memory.
testSet = readtable("testSet.csv");
N = 567*15;
result = [];
for chan=2:7
    param = params(chan-1,:)
    p = param(1);
    d = param(2);
    q = param(3);
    Mdl = arima(p,d,q);
    Mdl.Seasonality = 567-p;
    fitData = table2array(testSet(1:N,chan));
    EstMdl = estimate(Mdl,fitData, 'Display','off');
    for n=1:11907
        n
        iData = table2array(testSet( N+n : N+567+n, chan));

        result(n,chan-1) = forecast(EstMdl, 1, iData);
    end
end
writematrix(result,'arimaResult.csv')

writematrix(params,'Params.csv')


