import numpy as np
import pandas as pd
from pandas import DataFrame
from sklearn.preprocessing import StandardScaler, MinMaxScaler
import torch #pytorch
import torch.nn as nn
from torch.autograd import Variable
import matplotlib.pyplot as plt
import datetime as dt
import csv
import time
import random
from torch.utils.data import Dataset
from torch.utils.data import DataLoader

TEST_SETS = 3

device = 'cuda' if torch.cuda.is_available() else 'cpu'

# ---------- Hyperparameter Class ---------- #
class Hyperparameter:
    """
    Class for a hyperparameter, giving it 3 attributes:
        - name
        - default value
        - The sequence to iterate through when modifying the parameter.
    """

    def __init__(self, name, default, sequence = []):
        self.name = name
        self.default = default
        self.sequence =  sequence

# Hyperparameter objects

#Constant
INPUT_SIZE =  Hyperparameter("INPUT_SIZE", 6)
OUTPUT_SIZE =  Hyperparameter("OUTPUT_SIZE", 6)

# Dynamic
BATCH_SIZE =  Hyperparameter("BATCH_SIZE", 32, [32, 64, 128])
LEARNING_RATE =  Hyperparameter("LEARNING_RATE", 0.0001, [pow(10,-6), pow(10,-5), pow(10,-4), pow(10,-3), pow(10,-2), 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9])
MOMENTUM = Hyperparameter("MOMENTUM", 0.99, [0.5, 0.9, 0.99])
DROPOUT_RATE =  Hyperparameter("DROPOUT_RATE", 0.2, [0.2, 0.3, 0.4, 0.5])
NUM_LAYERS =  Hyperparameter("NUM_LAYERS", 4, [1,2,3,4])
ACTIVATION_FUNCTION = Hyperparameter("ACTIVATION_FUNCTION", "Sigmoid", ["ReLU", "Tanh", "Sigmoid"])
SEQUENCE_LENGTH = Hyperparameter("SEQUENCE_LENGTH", 567, [270, 567])
HIDDEN_SIZE = Hyperparameter("HIDDEN_SIZE", 400, [25, 50, 100, 200, 400, 800])
EPOCH_AMOUNT = Hyperparameter("EPOCH_AMOUNT", 22)

class Hyperparameters:
    """
    Class of all LSTM hyperparameters
    """
    BatchSize = BATCH_SIZE
    learningRate = LEARNING_RATE
    dropoutRate = DROPOUT_RATE
    numLayers = NUM_LAYERS
    inputSize = INPUT_SIZE
    outputSize = OUTPUT_SIZE
    actFnc = ACTIVATION_FUNCTION
    momentum = MOMENTUM
    sequenceLength = SEQUENCE_LENGTH
    hiddenSize = HIDDEN_SIZE
    epochAmount = EPOCH_AMOUNT

    def __init__ (self, optimizerType):

        #Set variables according to given optimizer
        if (optimizerType == "Adam"):
            self.optimizer = Hyperparameter("OPTIMIZER", "Adam")
        elif (optimizerType == "SGD"):
            self.optimizer = Hyperparameter("OPTIMIZER", "SGD")


class LSTM(nn.Module):
    """
    PyTorch LSTM class
    """
    # General LSTM network architecture
    def __init__(self, outputSize, inputSize, hiddenSize, numLayers, seqLength, dRate, actFnc):
        super().__init__()
        self.hiddenLayerSize = hiddenSize # create an attribute called hidden_layer_size
        self.numOfLayers = numLayers
        self.seqLength = seqLength
        self.lstm = nn.LSTM(input_size=inputSize, hidden_size=hiddenSize, num_layers=numLayers, batch_first=True) # create an attribute called lstm, which takes the value of an LSTM layer.
        self.dropout = nn.Dropout(p=dRate)
        self.linear = nn.Linear(hiddenSize, outputSize) # create an attribute called linear which takes the value of a linear layer.

        # Set activation function according to given argument
        if(actFnc == "ReLU"):
            self.activation = nn.ReLU()
        elif(actFnc == "Tanh"):
            self.activation = nn.Tanh()
        elif(actFnc == "Sigmoid"):
            self.activation = nn.Sigmoid()

    # Forward pass network architecture
    def forward(self, inputSeq):
        h0 = Variable(torch.zeros(self.numOfLayers, inputSeq.shape[0], self.hiddenLayerSize)).to(device) #hidden state
        c0 = Variable(torch.zeros(self.numOfLayers, inputSeq.shape[0], self.hiddenLayerSize)).to(device) #internal state
        # Propagate input through LSTM
        inputSeq = inputSeq.to(device)
        output, (hn, cn) = self.lstm(inputSeq, (h0, c0))
        hn = self.dropout(hn)
        hn = hn.view(-1, self.hiddenLayerSize) #Reshaping LSTM output for the Dense layer
        out = self.activation(hn)
        out = self.linear(out)
        if(out.shape[0] == NUM_LAYERS.default):
            return out[-1:]
        else:
            return out[-BATCH_SIZE.default:]


class Timeseries(Dataset):
    """
    Class used to convert data into a pyTorch dataset.
    """

    def __init__(self, x, y):
        self.x = Variable(torch.Tensor(x))#.to(device)
        self.y = Variable(torch.Tensor(y))#.to(device)

    def __getitem__(self, index):
        return (self.x[index], self.y[index])

    def __len__(self):
        return len(self.x)

# --------------------------------------- #

def splitSequences(dataSet, sampleLength):
    """
    Preprocess the data by splitting the data into two list:
        x: array of samples
        y: array of target values

    Sample_i in array x corresponds to target value_i in array y

    Args:
        dataSet(pd.DataFrame): The dataset to be preprocessed
        sampleLength (int): The desired length of a sample

    Return:
        x (np.array): Input data from the data set.
        y (np.array): Target values from the data set.
    """

    x, y = list(), list()
    for i in range(len(dataSet) - sampleLength - 1):
        sampleX = np.array(dataSet[i : i+sampleLength, :])
        sampleY = np.array(dataSet[i+sampleLength])
        x.append(sampleX)
        y.append(sampleY)

    return np.array(x), np.array(y)

def scale(data):
    """
    Scale data according to activation function used in the output layer.
    For tanH scale to range -1 to 1, otherwise 0 to 1 (ReLu and Sigmoid)

    Args:
        data (np.array): Target vectors to be scaled.
    Return:
        data (np.array): Scaled target vectors.
        mms (MinMaxScaler): Scaler used to scale the target vectors.
    """
    if(ACTIVATION_FUNCTION.default == "Tanh"):
        mms = MinMaxScaler(feature_range=(-1,1))
    else:
        mms = MinMaxScaler()

    return mms.fit_transform(data), mms

def setHypSequence(hyperparameters):
    """
    Get sequence in which the hyperparameters should be modified.
    There are two sequences as it depends on the optimiser used.

    Args:
        hyperparameters (hyperparameters): Object containing all hyperparameters to be modified.
    Return:
        sequence of model params (list): Predetermined sequence
    """
    if(hyperparameters.optimizer.default == "SGD"):
        return [hyperparameters.learningRate, hyperparameters.BatchSize, hyperparameters.momentum,
                hyperparameters.actFnc, hyperparameters.dropoutRate,
                hyperparameters.hiddenSize, hyperparameters.sequenceLength, hyperparameters.numLayers,
                hyperparameters.learningRate, hyperparameters.BatchSize]

    if(hyperparameters.optimizer.default == "Adam"):
        return [hyperparameters.learningRate, hyperparameters.BatchSize, hyperparameters.actFnc,
                hyperparameters.dropoutRate, hyperparameters.hiddenSize, hyperparameters.sequenceLength,
                hyperparameters.numLayers, hyperparameters.learningRate, hyperparameters.BatchSize]



def prepareModel(hyperparameters):
    """
    Prepares a model by defining the forward and backward pass of the model and initializes the loss function and optimizer.

    Args:
        hyperparameters (Hyperparameters): Object containing all hyperparameters defined in the top.

    Return:
        model (LSTM object): The prepared LSTM Model.
        lossFnc (nn.MSEloss): The loss function to be used.
        optimizer (torch.optim): The optimiser to be used.

    """


    # Define forward pass
    model = LSTM(hyperparameters.outputSize.default, hyperparameters.inputSize.default,
                 hyperparameters.hiddenSize.default, hyperparameters.numLayers.default,
                 hyperparameters.BatchSize.default, hyperparameters.dropoutRate.default,
                 hyperparameters.actFnc.default).to(device)

    # Define backward pass
    lossFnc = nn.MSELoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=hyperparameters.learningRate.default)
    if(hyperparameters.optimizer.default == "SGD"):
        optimizer = torch.optim.SGD(model.parameters(), lr=hyperparameters.learningRate.default, momentum=hyperparameters.momentum.default)

    return model, lossFnc, optimizer


def prepareData(name, hyperparameters):
    """
    Prepare a dataset, given as argument, by standardizing and preprocessing it into a suitable format.

    Args:
        name (string): File name of the .csv file containing the desired data.
        hyperparameters (Hyperparameters): Object containing all hyperparameters defined in the top.

    Return:
        trainLoader (DataLoader): A container which contains training data in a certain structure suitable for training.
        ss (StandardScaler): Standardized scaler fitted to the trainning set.
        mms (MinMaxScaler): MinMax Scaler fitted to the target set.
    """

    # Prepare training data
    dataset = pd.read_csv('%s.csv' % name, index_col='timestamp', parse_dates=True)
    # Standardize dataset
    ss = StandardScaler()
    dataSS = ss.fit_transform(dataset)
    # Preprocess the data
    samples, targets = splitSequences(dataSS, hyperparameters.sequenceLength.default)
    targetsMMS, mms = scale(targets)
    dataset = Timeseries(samples, np.array(targetsMMS))
    dataLoader = DataLoader(dataset, shuffle=True, batch_size=BATCH_SIZE.default)

    return dataLoader, ss, mms


def earlyStopping(hyperparameters, trainLoader, validationLoader, optim, lossFnc, model, mms, ss):
    """
    Train a model with early stopping.

    Args:
        model(pyTorch Attr):  The model to be trained. The model is copied by reference. Thus, any changes to the model in
                              this function impacts the actual model.
        optim (pyTorch Attr): The optimizer chosen for the model
        lossFnc(pyTorch Attr):The loss function chosen for the model
        hyperparameters (Hyperparameters): Object containing all hyperparameters to be modified.
        trainLoader (DataLoader): A container which contains training data in a certain structure suitable for training.
        ss (StandardScaler): Standardized scaler fitted to the trainning set.
        mms (MinMaxScaler): MinMax Scaler fitted to the target set.
    Return:
        bestEpoch (int): The epoch value determined by the early stopping process to be the optimal.
    """
    minValLoss = 1000 # Variable to hold minimum validation loss
    checkInterval = 2 # Frequency of validating the model
    patience = 4 # The patience introduced before stopping training
    checksNoImp = 0 # Number of times the loss has not improved (Measured in # of checkIntervals)
    bestEpoch = 2 # Best found epoch value
    valLoss = 100
    for epoch in range(10000): # Number of maximum epochs. This will not be reached due to early stopping.
        startTime = time.time()
        # Train on all training data
        for batch, target in trainLoader:
            if(batch.shape[0] < BATCH_SIZE.default):
                break
            optim.zero_grad()
            prediction = model(batch.to(device))
            loss = lossFnc(prediction, target.to(device))
            loss.backward()
            optim.step()
        # Checkpoint every n th epoch
        if (epoch > 1) and (epoch % checkInterval == 0):
            valLoss = validateModel(validationLoader, model, lossFnc, ss, mms, hyperparameters, False)
            model.train()

        # Update values if current model has better validation loss
            if valLoss < minValLoss:
                checksNoImp = 0
                minValLoss = valLoss
                bestEpoch = epoch
            else:
                checksNoImp += 1

        # Break training loop when patience has been exceeded
        if checksNoImp == patience:
            break
        print("Epoch: %d, loss: %1.5f" % (epoch, valLoss), "| Epoch execution time: %1.10f " % (time.time() - startTime), end='\r')
    print('\r')
    return bestEpoch


def trainModel(hyperparameters, trainLoader, optim, lossFnc, model):
    """
        The model trains by forward passing a batch of samples. After each forward pass of a bacth, a backward pass is executed.
        This is repeated for the given amount of epochs.

        Args:
            model (LSTM): The model to be trained. The model is copied by reference. Thus, any changes to the model in
                          this function impacts the actual model.
            optim (pyTorch Attr): The optimizer chosen for the model
            lossFnc(pyTorch Attr):The loss function chosen for the model
            hyperparameters (Hyperparameters): Object containing all hyperparameters to be modified.
            trainLoader (DataLoader): A container which contains training data in a certain structure suitable for training.
        Return:
            loss (float): Most recent error between target and prediction
    """

    for i in range(hyperparameters.epochAmount.default):
        startTime = time.time()
        for batch, target in trainLoader:
            if(batch.shape[0] < BATCH_SIZE.default):
                break
            optim.zero_grad()
            prediction = model(batch.to(device))
            loss = lossFnc(prediction, target.to(device))
            loss.backward()
            optim.step()

        # Print
        if i % 1 == 0:
            print("Epoch: %d, loss: %1.5f" % (i, loss.item()), "| Epoch execution time: %1.10f " % (time.time() - startTime), end='\r')
    print('\r')
    return loss.item()

def validateModel(validationLoader, model, lossFnc, ss, mms, hyperparameters, final):
    """
    Validate the model with a validation set using walk forward validation

    Args:
        ValidationLoader (DataLoader): A container which contains validation data in a certain structure suitable for the validation process.
        model (LSTM): The model to be trained. The model is copied by reference.
                      Thus, any changes to the model in this function impacts the actual model.
        lossFnc (pyTorch Attr): The loss function chosen for the model
        ss (StandardScaler): Standardized scaler fitted to the trainning set.
        mms (MinMaxScaler): MinMax Scaler fitted to the target set.
        hyperparameters (Hyperparameters): Object containing all hyperparameters to be modified.
        final (boolean): A variable used to determine if the predictions should be saved and returned.

    Return:
        averageLoss (float): The average error calculated over the entire validation set.
        predictions (list): The predictions made by the model using the validation set as inputs.

    """
    totalLoss = 0
    iterator = 0
    predictions = []
    model.eval()
    for batch, target in validationLoader:
        with torch.no_grad():
            if batch.shape[0] < BATCH_SIZE.default:
                break
            prediction = model(batch.to(device))
            loss = lossFnc(prediction, target.to(device)) # returns the average MSE of all entries in batch and targets
            totalLoss += loss.item()
            iterator += 1

            # Append predictions to array of predictions
            if final == True:

                predictionList = prediction.tolist()
                invTransformedPredictions = ss.inverse_transform(mms.inverse_transform(predictionList))
                for i in range(len(invTransformedPredictions)):
                    predictions.append(invTransformedPredictions[i])

    averageLoss = totalLoss / iterator

    # Return
    if final == True:
        return averageLoss, predictions
    else:
        return averageLoss

def hpTuning(hyperparameter, hyperparameters):
    """
    Tunes a hyperparameter within a given range of values.

    Args:
        hyperparameter (Hyperparameter): The hyperparameter to be tuned.
        hyperparameters (Hyperparameters): The hyperparameter values of the model.

    """
    print("\n---------- " + hyperparameter.name + " -----------")
    hyperparamTime = time.time()
    hyperparamLoss = [] # List to contain loss of different hyperparameter values
    lowestLoss = 100 # init lowestloss higher than any possible loss value
    index = 0 # init index
    for j in range(len(hyperparameter.sequence)):
        paramValueTime = time.time()
        hyperparameter.default = hyperparameter.sequence[j]
        print("\n%s with value:" % hyperparameter.name, hyperparameter.default)

        # Prepare, train and validate model
        model, lossFnc, optimizer = prepareModel(hyperparameters)
        trainLoader, trainSS, trainMMS = prepareData("trainingSet", hyperparameters)
        trainModel(hyperparameters, trainLoader, optimizer, lossFnc, model)
        validationLoader, validationSS, validationMMS = prepareData("validationSet", hyperparameters)
        loss = validateModel(validationLoader, model, lossFnc, validationSS, validationMMS, hyperparameters, False)

        # Append to list of all loss values for the current tested parameter
        hyperparamLoss.append(loss)
        print("Model execution time: %1.10f" % (time.time() - paramValueTime))

    # Find index of value in list with lowest loss
    for j in range(len(hyperparamLoss)):
        if(hyperparamLoss[j] < lowestLoss):
            lowestLoss = hyperparamLoss[j]
            index = j
    # update default value of parameter
    hyperparameter.default = hyperparameter.sequence[index]
    print("\nHyperparameter execution time: %1.10f" % (time.time() - hyperparamTime), "| Lowest testing loss: %1.10f with value:" % loss, hyperparameter.default)


def findOptimModel(optimizer):
    """
    Finds the optimal hyperparameter values for a given optimiser using a predetermined sequence of hyperparameters.
    Once the optimal values are found, a model trained using these is saved along with its predictions.

    Args:
        optimizer (pyTorch Attr): The optimizer chosen for the model
    """
    # Set optimizer
    hyperparameters = Hyperparameters(optimizer)

    # Find optimal epoch amount with early stopping by using default hyperparameter values
    model, lossFnc, optimizer = prepareModel(hyperparameters)
    trainLoader, trainSS, trainMMS = prepareData("trainingSet", hyperparameters)
    validationLoaderES, ss, mms = prepareData("validationSetES", hyperparameters)
    hyperparameters.epochAmount.default = earlyStopping(hyperparameters, trainLoader, validationLoaderES, optimizer, lossFnc, model, mms, ss)

    # Tune hyperparameters
    order = setHypSequence(hyperparameters)
    print(hyperparameters.epochAmount.default)
    for hyperparam in order:
        hpTuning(hyperparam, hyperparameters)

    # Find optimal epoch amount using early stopping
    model, lossFnc, optimizer = prepareModel(hyperparameters)
    trainLoader, trainSS, trainMMS = prepareData("trainingSet", hyperparameters)
    validationLoaderES, ss, mms = prepareData("validationSetES", hyperparameters)
    hyperparameters.epochAmount.default = earlyStopping(hyperparameters, trainLoader, validationLoaderES, optimizer, lossFnc, model, mms, ss)

    # Train final model
    model, lossFnc, optimizer = prepareModel(hyperparameters)
    trainLoader, trainSS, trainMMS = prepareData("traininSet", hyperparameters)
    validationLoader, ss, mms = prepareData("validationSetFinal", hyperparameters)
    trainModel(hyperparameters, trainLoader, optimizer, lossFnc, model)
    loss, predictions = validateModel(validationLoader, model, lossFnc, ss, mms, hyperparameters, True)

    # Save predictions to a csv file
    predToCsv = pd.DataFrame(data = predictions, columns=("chan0", "chan1", "chan2", "chan3", "chan4", "chan5"))
    predToCsv.to_csv(str(hyperparameters.optimizer.default) + "_" + "predictions.csv", index=False)
    print("save")

    # Save model
    if hyperparameters.optimizer.default == "Adam":
        pthName =   ("LSTMFinal_" + "_" + str(hyperparameters.optimizer.default) + "_" +
                    str(hyperparameters.epochAmount.default) + "_" + str(hyperparameters.BatchSize.default) + "_" +
                    str(hyperparameters.learningRate.default) + "_" + str(hyperparameters.dropoutRate.default) + "_" +
                    str(hyperparameters.numLayers.default) + "_" + str(hyperparameters.hiddenSize.default) + "_" +
                    str(hyperparameters.actFnc.default) + "_" + str(loss) + ".pth")
        torch.save(model.state_dict(), pthName)
        print("saved")
    else:
        pthName =   ("LSTMFinal_"+ "_" + str(hyperparameters.optimizer.default) + "_" +
                    str(hyperparameters.epochAmount.default) + "_" + str(hyperparameters.BatchSize.default) + "_" +
                    str(hyperparameters.learningRate.default) + "_" + str(hyperparameters.dropoutRate.default) + "_" +
                    str(hyperparameters.numLayers.default) + "_" + str(hyperparameters.hiddenSize.default) + "_" +
                    str(hyperparameters.actFnc.default) + "_" + str(hyperparameters.momentum.default) + "_" + str(loss) + ".pth")
        torch.save(model.state_dict(), pthName)
        print("Saved")

def trainTestFinalModel(optimizer):
    """
    Train model with both training and validation set, and test model with test set.

    Args:
        optimizer (pyTorch Attr): The optimizer chosen for the model

    """

    hyperparameters = Hyperparameters(optimizer)

    model, lossFnc, optimizer = prepareModel(hyperparameters)
    trainLoader, trainSS, trainMMS = prepareData("validationAndTrainingSet", hyperparameters)
    testLoader, ss, mms = prepareData("testSet", hyperparameters)
    trainModel(hyperparameters, trainLoader, optimizer, lossFnc, model)
    loss, predictions = validateModel(testLoader, model, lossFnc, ss, mms, hyperparameters, True)

    # Save predictions to a csv file
    predToCsv = pd.DataFrame(data = predictions, columns=("chan0", "chan1", "chan2", "chan3", "chan4", "chan5"))
    predToCsv.to_csv("Results.csv", index=False)
    print("save")

    # Save model
    pthName = ("FinalModel" + "_" + str(loss) + ".pth")
    torch.save(model.state_dict(), pthName)
    print("Saved")

# ----------- Main ---------- #

# Find the most optimal hyperparameters for given epoch amount and desired optimizer.
print("START")
findOptimModel("Adam")
print("model done")
#findOptimModel("SGD")
