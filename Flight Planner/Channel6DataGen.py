import csv
from array import *
import matplotlib.pyplot as plt
import numpy as np
import random
import time as clock

random.seed(clock.time())

# :--| Explaination |--:
# one period is sepparated into three 'phases'
# rising, flat and decreasing
# one period has a baseline value
# when before the rise or decrease a group is chosen
# this group determines how fast the increase or decrease happens
# :--| Explaination over |--:

periodLength = 1165.25429424944

baseLine = 0.75 # the 'normal' baseline value for the data

# the three groups
# first value is acceleration
# the second value is 'chance' of that group being picked
lG = [1.145, 12.41]
mG = [1.246, 35.18]
hG = [1.356, 52.41]

numbOfPeriods = 1000
resolution = 10 # seconds between samples

def getGroup(val):
    if(val < lG[1]):
        variance = random.uniform(-0.03,0.03)
        return lG[0] + variance
    elif(val >= lG[1] and val < (lG[1] + mG[1])):
        variance = random.uniform(-0.04,0.04)
        return mG[0] + variance
    elif(val >= (lG[1] + mG[1])):
        variance = random.uniform(-0.05,0.05)
        return hG[0] + variance
    else:
        print("error" , val)
        return baseLine

def createNoise(val):
    noise = random.uniform(-0.01,0.01)
    return val + noise

data = []
x = 0
X = []
groupVal = 0

for i in range(numbOfPeriods):
    #add base line value
    data.append(createNoise(baseLine))
    X.append(x)
    x += resolution
    
    #create rising values going to randomly selected group
    groupVal = getGroup(random.uniform(0,100))

    risingEntries = int((periodLength/3)/resolution)
    increaseStep = (groupVal - baseLine)/risingEntries
    nextVal = baseLine + increaseStep
    
    # generating the rising values
    for rising in range(risingEntries):
        data.append(nextVal)
        nextVal += increaseStep
        X.append(x)
        x += resolution
    
    #generating the flat values
    for flat in range(int((periodLength/3)/resolution)):
        data.append(createNoise(groupVal))
        X.append(x)
        x += resolution

    decreasingEntries = int((periodLength/3)/resolution)
    decreaseStep = increaseStep
    nextVal = groupVal - decreaseStep
    
    # generating the decreasing values
    for decreasing in range(decreasingEntries):
        data.append(nextVal)
        nextVal -= decreaseStep
        X.append(x)
        x += resolution

#saving data to csv format
with open('Channel6Consumption10FreqWithVarianceThirdFlat.csv', 'w', newline='') as dataFile:
    writer = csv.writer(dataFile)
    writer.writerow(["Watt"])
    for i in range(len(data)):
        writer.writerow([data[i]])

plt.plot(X, data, label="third")
plt.show()
