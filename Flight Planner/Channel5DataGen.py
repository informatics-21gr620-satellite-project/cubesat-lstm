import csv
from array import *
import matplotlib.pyplot as plt
import numpy as np
import random
import time as clock

# :--| Explaination |--:
# one period is sepparated into two 'phases'
# up tim and down time
# during down time, the gain is at a baseline (with some added noise)
# when generating the up time a group is randomly picked
# a group is simply a baseline value that it different from the normal baseline
# :--| Explaination over |--:

random.seed(clock.time())

dT = 3670 # up time in seconds
uT = 2005 # down time in seconds

baseLine = 1.4 # the 'normal' baseline value for the data

# the four groups, picked randomly at the start of each up time
# the first value is the base line value for the groupVal
# the second value is the percentage 'chance' that group is chosen
lG = [2.4, 33.58] #low group
mG = [3.7, 42] # medium group
sHG = [8.1, 12.52] # semi high group
hG = [13.8, 11.9] # high group

numbOfPeriods = 1000
resolution = 10 # seconds between samples
offset = 0 # offsetting the starting point

def getGroup(val):
    # a random value between 0 and 100
    # this random number determines the group
    if(val < lG[1]):
        variance = random.uniform(-0.2,0.2)
        return lG[0] + variance
    elif(val >= lG[1] and val < lG[1] + mG[1]):
        variance = random.uniform(-0.2,0.2)
        return mG[0] + variance
    elif(val >= lG[1] + mG[1] and val < lG[1] + mG[1] + sHG[1]):
        variance = random.uniform(-0.3,0.3)
        return sHG[0] + variance
    elif(val >= lG[1] + mG[1] + sHG[1]):
        variance = random.uniform(-0.4,0.4)
        return hG[0] + variance
    else:
        return 1.4

def createNoise(val):
    noise = random.uniform(-0.1,0.2)
    return val+noise


data = []
X = [] #list of X values for plotting
x = 0 # initial value for x value


groupVal = getGroup(random.uniform(0,100))

for i in range(numbOfPeriods):
    # add base line values in down time
    for base in range(int(dT/resolution)):
        data.append(createNoise(baseLine))
        X.append(x)
        x += resolution
    
    groupVal = getGroup(random.uniform(0,100))
    
    #add uptime values
    for spike in range(int(uT/resolution)):
        data.append(createNoise(groupVal))
        
        #appending the next x value to the list
        X.append(x)
        x += resolution # generating a new x value

# write data to csv
with open('Channel5Consumption10FreqWithVariance.csv', 'w', newline='') as train_file:
    writer = csv.writer(train_file)
    writer.writerow(["Watt"])
    for i in range(len(data)):
        writer.writerow([data[i]])


plt.plot(X, data)
plt.show()
